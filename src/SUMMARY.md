# Summary

[Introduction](README.md)

# User Guide

- [Installation](guide/installation.md)
- [Building images](guide/building.md)
- [Systems administration](guide/admin.md)

# Background and architecture

- [Cloud agents](rationale/cloud-agents.md)